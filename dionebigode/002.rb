#https://soundcloud.com/user-894146376/002-1
#playing with play and stop buttoms

define :fbf2f do
  sample :drum_heavy_kick
  sleep 0.25
  sample :drum_cymbal_closed
  sleep 0.25
  sample :drum_heavy_kick
  sample :drum_snare_soft
  sleep 0.25
  sample :drum_cymbal_closed
  sleep 0.25
end

define :quick_pick do |n|
  play n, release: 0.2
end


define :bass do
  use_synth :chipbass
  quick_pick :e2
  sleep 0.25
  quick_pick:e2
  sleep 0.25
  quick_pick :g2
  sleep 0.25
  quick_pick :e2
  sleep 0.25
end

/youll need to copy and paste this as you play with the play and stop button/
in_thread do
  8.times do
    bass
  end
end

loop do
  16.times do
    fbf2f
  end
end


