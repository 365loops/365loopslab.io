#https://soundcloud.com/user-894146376/005-1
#final amen loop player, now with note selection

low_bound = 0
high_bound = 0.99
delta_low = 0.1
delta_high = 0.7
duration = sample_duration :loop_amen
splits = 16
rate = 0.7
beat_duration = (duration*rate)/8
note_32 = 0.03125
note_lengt = note_32 * 2

define :la_break do |start, finish|
  sample :loop_amen, start: start, finish: finish, rate: rate
end

define :hit_drum do |n|
  note = note_lengt * n
  la_break note, note + note_lengt
end

define :play_seq do |p|
x = 0
16.times do
  hit_drum p[x]
  sleep beat_duration
  x = x + 1
  if x > (splits -1)
    x = 0
  end
end
end

y = [0,3,6,3,4,3,6,3,0,0,3,6,4,5,5,4]
play_seq y
y = [0,3,6,4,4,9,6,3,10,10,3,6,4,5,5,4]
play_seq y