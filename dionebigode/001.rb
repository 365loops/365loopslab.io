#https://soundcloud.com/user-894146376/001-1
#first experiment, just some random stuff

sample :ambi_lunar_land, rate: -1

4.times do
  use_synth :saw
  play 36
  sleep 0.25
  play 48
  sleep 0.25
  play 60
  sleep 0.25
  play 48
end

4.times do
  v = rrand(36, 48)
  play :c2
  sleep 0.25
  play :c3
  sleep 0.25
  if one_in(2)
    play :c4
    sleep 0.25
  else
    play :c4
    sleep 0.25/4
    play v
    v = rrand_i(36, 48)
    sleep 0.25/4
    play :c4
    sleep 0.25/4
    play v
    sleep 0.25/4
  end
  play :c3
end

sample :ambi_lunar_land, rate: -1
4.times do
  if one_in(2)
    sample :drum_heavy_kick
    sleep 0.5
    v = rrand(36, 48)
    play :c4
    sleep 0.25
    play v
    v = rrand_i(36, 48)
    sleep 0.25
    play :c4
  else
    sample :drum_cymbal_closed
    sleep 0.25
  end
  sleep 0.25
  play v
  sleep 0.25
  play :c3
end

4.times do
  use_synth :saw
  play 36
  sample :drum_heavy_kick
  sample :drum_cymbal_closed
  sleep 0.25
  play 48
  sample :drum_cymbal_closed
  sleep 0.25
  play 60
  sample :drum_cymbal_closed
  sleep 0.25
  play 48
  if one_in(2)
    sample :drum_heavy_kick
    sleep 0.25
  else
  end
end

4.times do
  use_synth :saw
  play 41
  sample :drum_heavy_kick
  sample :drum_cymbal_closed
  sleep 0.25
  play 53
  sample :drum_cymbal_closed
  sleep 0.25
  play 65
  sample :drum_cymbal_closed
  sleep 0.25
  play 53
  if one_in(2)
    sample :drum_heavy_kick
    sleep 0.25
  else
  end
end

4.times do
  use_synth :saw
  play 39
  sample :drum_heavy_kick
  sample :drum_cymbal_closed
  sleep 0.25
  play 51
  sample :drum_cymbal_closed
  sleep 0.25
  play 63
  sample :drum_cymbal_closed
  sleep 0.25
  play 51
  if one_in(2)
    sample :drum_heavy_kick
    sleep 0.25
  else
  end
end


4.times do
  use_synth :saw
  play 36
  sample :drum_heavy_kick
  sample :drum_cymbal_closed
  sleep 0.25
  play 48
  sample :drum_cymbal_closed
  sleep 0.25
  play 60
  sample :drum_cymbal_closed
  sleep 0.25
  play 48
  if one_in(2)
    sample :drum_heavy_kick
    sleep 0.25
  else
  end
end