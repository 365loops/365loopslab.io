#https://soundcloud.com/user-894146376/004-1
#just a favorite experiment
#sadly i cant get it to sound like the recording anymore =(

define :la_break do |start, finish|
  sample :loop_amen, start: start, finish: finish, rate: 0.7
end

low_bound = 0
high_bound = 0.97
delta_low = 0.1
delta_high = 0.7
duration = sample_duration :loop_amen


in_thread do
  128.times do
    delta_high = delta_high - delta_high/32
    sleep 0.7
  end
end

define :hit_drum do |n|
  use_random_seed n
  div = rrand_i(4,16)
  note = (duration / div ) * duration
  start = rrand(low_bound,high_bound )
  finish = rrand(low_bound,high_bound )
  sfa = (start.abs-finish.abs).abs
  /
    while((sfa < delta_low) || (sfa > delta_high))
      start = rrand(low_bound,high_bound )
      finish = rrand(low_bound,high_bound )
      sfa = (start.abs-finish.abs).abs
    end
    /
  if start + note > 1
    la_break start, (start - note).abs
  else
    la_break start, start + note
  end
  print delta_high
  print sfa
  sleep note
end

define :rand_hit do |s,e|
  hit_drum rrand_i(s,e)
end

use_random_seed 83294
in_thread do
  
  128.times do
    
    gap = 0.16
    
    rand_hit(170,186)
    sleep gap
  end
end# Welcome to Sonic Pi v3.1

