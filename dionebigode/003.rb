#random amen loop break maker pt 1
#https://soundcloud.com/user-894146376/003-1

define :la_break do |start, finish|
  sample :loop_amen, start: start, finish: finish, rate: 1
end

low_bound = 0
high_bound = 0.97
delta_low = 0.1
delta_high = 0.7

in_thread do
  128.times do
    delta_high = delta_high - delta_high/32
    sleep 0.7
  end
end

in_thread do
  512.times do
    start = rrand(low_bound,high_bound )
    finish = rrand(low_bound,high_bound )
    sfa = (start.abs-finish.abs).abs
    
    while((sfa < delta_low) || (sfa > delta_high))
      start = rrand(low_bound,high_bound )
      finish = rrand(low_bound,high_bound )
      sfa = (start.abs-finish.abs).abs
    end
    
    if start > finish
      la_break finish, start
    else
      la_break start, finish
    end
    print delta_high
    print sfa
    sleep sfa
  end
end